var animation = lottie.loadAnimation({
    container: document.getElementById('smart'), // the dom element that will contain the animation
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'lottie/smart-data.json' // the path to the animation json
});
